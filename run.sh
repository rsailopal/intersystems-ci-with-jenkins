#!/bin/bash
#
#	AUTHOR - Raman Sailopal
#
#	Routine for continous integration/deployment
#	Jumps onto Intersystems Cache database server and creates a new database
#	and namespace from the current live one
#
#	This is simply and example and will have to be modified accordingly
#
if [ -e nextBuildNumber ]
then
	nxtbuild="build$(echo $(cat nextBuildNumber) - 1 | bc)";
else
	nxtbuild="build";
fi
ssh -i id_rsa ras@192.168.240.27 "cd /home/ras;./buildenv1.sh $nxtbuild"
exit;
