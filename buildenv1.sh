#!/bin/bash
#
#	AUTHOR - Raman Sailopal
#
#	Script to automatically create a new Cache namespace environment for 
#	doevelopment/testing. Works with Jenkins and is initiated from the 
#	Jenkins server 
#
cmd() {
	sleep 2;
	echo 'ZN "%SYS"';
	sleep 2;
	echo "D BUILD^NEWENV(\""$dr"\"\,\""$rootpath"\"\,\"$rootdir"\"\,\"$cspsir"\"\,\""$applpref")";
}
cmd1() {
	sleep 2;
	echo "ZN \""$dr"\"";
	sleep 2;
	echo "H";
}
cmd2() {
	sleep 2;
	echo "ZN \""$dr"\"";
	sleep 2;
	echo "Do \$SYSTEM.CSP.LoadPageDir(\""$cspdir$dr"/\",\"c\")";
	sleep 2;
	echo "h";
}
#
# The next four parameters are specific to your Cache environment and so change
# change accordingly
#
cspdir="/rbl7/csp/" # The direcitry where the csp files are stored
cinstance="RBL7" # The name created for your cache instance when Cache was first# installed
rootdir="rbl" # Main directory where the core files for a new environment will # be created from
rootpath="/rbl7/rblsys/" # The full path of the parent directory of where the 
# Cache DAT files are stored
applpref="/csp/" # The prefix for the csp applications
#
#
#
dr=$1;
mkdir -m 775 $cspdir$dr;
cmd|csession $cinstance -B;
#
# OK Now check that we can switch to the namespace we have just created
#
cmd1|csession $cinstance -B | grep "<NAMESPACE>";
if [ "$?" == "0" ]
then
	exit 1;
else
	cp -R $cspdir$rootdir $cspdir$dr;
	chmod -R 775 $cspdir$dr;
	cmd2|csession $cinstance -B;
	exit 0;
fi
